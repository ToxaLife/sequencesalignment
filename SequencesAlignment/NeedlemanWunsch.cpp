//
//  NeedlemanWunsch.cpp
//  SequencesAlignment
//
//  Created by Александр Поповкин on 30/10/15.
//  Copyright © 2015 AlexStudio. All rights reserved.
//

#include "NeedlemanWunsch.hpp"

using namespace std;

void NeedlemanWunsch::FindAlingment() {
    string align_a = "", align_b = "";
    
    build_score_matrix(false);
    
    int i = (int)score_matrix.size1() - 1;
    int j = (int)score_matrix.size2() - 1;
    result_score = score_matrix(i, j).score;
    while (i > 0 || j > 0) {
        int score = score_matrix(i, j).score;
        int _x = score_matrix(i, j).x;
        int _y = score_matrix(i, j).y;
        
        if (i > 0 && j > 0 && score == score_matrix(i - 1, j - 1).score + compare_input_strings(i - 1, j - 1)) {
            align_a = input_string_1[--i] + align_a;
            align_b = input_string_2[--j] + align_b;
            
        }
        else if (i > 0 && score == _y) {
            align_a = input_string_1[--i] + align_a;
            align_b = "-" + align_b;
            
        }
        else if (j > 0 && score == _x){
            align_a = "-" + align_a;
            align_b = input_string_2[--j] + align_b;
        }
    }
    
    result_string_1 =  align_a;
    result_string_2 =  align_b;
    
}
