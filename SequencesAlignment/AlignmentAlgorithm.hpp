//
//  AlingmentAlgorithm.hpp
//  SequencesAlignment
//
//  Created by ToxaLife on 30/10/15.
//  Copyright © 2015 AlexStudio. All rights reserved.
//

#ifndef AlingmentAlgorithm_hpp
#define AlingmentAlgorithm_hpp

#include <iostream>
#include <fstream>
#include <string>
#include <limits>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>

typedef struct _matrix_field {
    int score, x, y;
    void print() {
        std::cout << "{" << score << "," << x << "," << y << "}";
    }
} matrix_field;

typedef boost::numeric::ublas::matrix<matrix_field> matrix_type;

class AlignmentAlgorithm {
private:
    int affine_gap(int length);
protected:
    std::string input_string_1, input_string_2;
    std::string result_string_1, result_string_2;
    matrix_type score_matrix;
    
    int match, mismatch, gap_start, gap_extension, result_score;
    
    int compare_input_strings(int i, int j);
    void build_score_matrix(bool local);
    
public:
    
    virtual void FindAlingment() = 0;
    bool OpenFile(std::string filepath);
    AlignmentAlgorithm(int match, int mismatch, int gap_start, int gap_extension);
    ~AlignmentAlgorithm();
    
    std::string get_result_string_1();
    std::string get_result_string_2();
    int get_result_score();
};

#endif /* AlingmentAlgorithm_hpp */
