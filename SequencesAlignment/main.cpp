//
//  main.cpp
//  SequencesAlignment
//
//  Created by Александр Поповкин on 30/10/15.
//  Copyright © 2015 AlexStudio. All rights reserved.
//
#include "Header.h"

using namespace std;

int main(int argc, const char * argv[]) {
    
    
    string filepath = "/Users/ToxaLife/SequencesAlignment/dna.txt";
    cout << "Needleman-Wunch(n) or Smith-Waterman(s)\n";
    char answer;
    while (true) {
        cin >> answer;
        if (answer == 'n') {
            cout << "Needleman Wunsch algorithm\n";
            NeedlemanWunsch *needleman_wunsch = new NeedlemanWunsch(MATCH_NUMBER, MISMATCH_NUMBER, GAP_OPEN, GAP_EXTEND);
            
            if (!needleman_wunsch->OpenFile(filepath)) {
                return 0;
            }
            
            needleman_wunsch->FindAlingment();
            
            cout << "Score:" << needleman_wunsch->get_result_score() << endl;
            cout << "Result sequences:\n";
            cout << "Seq 1 -> " + needleman_wunsch->get_result_string_1() + "\n";
            cout << "Seq 2 -> " + needleman_wunsch->get_result_string_2() + "\n";
        }else if (answer == 's') {
            cout << "Smith Waterman algorithm\n";
            SmithWaterman *smith_waterman = new SmithWaterman(MATCH_NUMBER, MISMATCH_NUMBER, GAP_OPEN, GAP_EXTEND);
            
            if (!smith_waterman->OpenFile(filepath)) {
                return 0;
            }
            
            smith_waterman->FindAlingment();
            
            cout << "Score:" << smith_waterman->get_result_score() << endl;
            cout << "Result sequences:\n";
            cout << "Seq 1 -> " + smith_waterman->get_result_string_1() + "\n";
            cout << "Seq 2 -> " + smith_waterman->get_result_string_2() + "\n";
        } else {
            break;
        }
    }
    cout << "Finished\n";
    return 0;
}

