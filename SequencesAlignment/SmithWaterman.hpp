//
//  SmithWaterman.hpp
//  SequencesAlignment
//
//  Created by Александр Поповкин on 30/10/15.
//  Copyright © 2015 AlexStudio. All rights reserved.
//

#ifndef SmithWaterman_hpp
#define SmithWaterman_hpp



#include "AlignmentAlgorithm.hpp"

class SmithWaterman:public AlignmentAlgorithm {
    
    using AlignmentAlgorithm::AlignmentAlgorithm;
public:
    void FindAlingment();
private:
    std::tuple<int, int, int> max_matrix();
};

#endif /* SmithWaterman_hpp */
