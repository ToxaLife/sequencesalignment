//
//  AlingmentAlgorithm.cpp
//  SequencesAlignment
//
//  Created by ToxaLife on 30/10/15.
//  Copyright © 2015 AlexStudio. All rights reserved.
//

#include "AlignmentAlgorithm.hpp"


using namespace std;

bool AlignmentAlgorithm::OpenFile(string filepath) {
    bool status = true;
    ifstream dna_file;
    stringstream dna_string_stream;
    string dna_string;
    boost::regex pattern(">seq1([A-Z]*)>seq2([A-Z]*)");
    boost::smatch result;
    dna_file.exceptions(ifstream::failbit);
    
    try {
        dna_file.open(filepath);
        
        dna_string_stream << dna_file.rdbuf();
        dna_string = dna_string_stream.str();
        boost::erase_all(dna_string, " ");
        boost::erase_all(dna_string, "\n");
        boost::regex_search(dna_string, result, pattern);
        
        if (result.size() == 3 && !(result[0] == "")) {
            input_string_1 = result[1];
            input_string_2 = result[2];
        } else {
            throw string("Sequences not found");
        }
        
        
        cout << "2 Sequences found:\n";
        cout << "Seq 1 -> " + input_string_1 + "\n";
        cout << "Seq 2 -> " + input_string_2 + "\n\n";
        
        dna_file.close();
    } catch (ifstream::failure e) {
        cerr << "Exeption file open/read/close\n";
        status = false;
    } catch (string str) {
        cerr << str << "\n";
        status = false;
        
    }
    if (dna_file.is_open()) {
        dna_file.close();
    }
    
    return status;
}
int AlignmentAlgorithm::affine_gap(int length) {
    return length > 0 ? gap_start + (length - 1) * gap_extension : 0;
}
void AlignmentAlgorithm::build_score_matrix(bool local) {
    
    int len1 = (int)input_string_1.length();
    int len2 = (int)input_string_2.length();
    int minus_inf = -numeric_limits<int>::max();
    score_matrix = matrix_type (len1 + 1, len2 + 1);
    
    score_matrix(0, 0) = {0, minus_inf, minus_inf};
    if (local) {
        for (int i = 1; i <= len1; i++) {
            score_matrix(i, 0) = {0, minus_inf, 0};
        }
        
        for (int i = 1; i <= len2; i++) {
            score_matrix(0, i) = {0, 0, minus_inf};
        }

    } else {
        for (int i = 1; i <= len1; i++) {
            score_matrix(i, 0) = {affine_gap(i), minus_inf, affine_gap(i)};
        }
        
        for (int i = 1 ; i <= len2; i++) {
            score_matrix(0, i) = {affine_gap(i), affine_gap(i), minus_inf};
        }

    }
    
    for (int i = 1; i <= len1; i++) {
        for (int j = 1; j <= len2; j++) {
            int _match = score_matrix(i - 1, j - 1).score + compare_input_strings(i - 1, j - 1);
            int _x = max(score_matrix(i, j - 1).score + gap_start, score_matrix(i, j - 1).x + gap_extension);
            int _y = max(score_matrix(i - 1, j).score + gap_start, score_matrix(i - 1, j).y + gap_extension);
            
            if (local) {
                score_matrix(i, j) = {max({_match, _x, _y, 0}), _x, _y};
            } else {
                score_matrix(i, j) = {max({_match, _x, _y}), _x, _y};
            }
        }
    }
    cout << "Score matrix is built\n";
}
string AlignmentAlgorithm::get_result_string_1() {
    return result_string_1;
}

string AlignmentAlgorithm::get_result_string_2() {
    return result_string_2;
}

int AlignmentAlgorithm::get_result_score() {
    return result_score;
}

int AlignmentAlgorithm::compare_input_strings(int i, int j) {
    return (input_string_1[i] == input_string_2[j]) ? match : mismatch;
}

AlignmentAlgorithm::AlignmentAlgorithm(int match, int mismatch, int gap_start, int gap_extension) {
    this->match = match;
    this->mismatch = mismatch;
    this->gap_start = gap_start;
    this->gap_extension = gap_extension;
}
AlignmentAlgorithm::~AlignmentAlgorithm() {
}