//
//  NeedlemanWunsch.hpp
//  SequencesAlignment
//
//  Created by Александр Поповкин on 30/10/15.
//  Copyright © 2015 AlexStudio. All rights reserved.
//

#ifndef NeedlemanWunsch_hpp
#define NeedlemanWunsch_hpp


#include "AlignmentAlgorithm.hpp"

class NeedlemanWunsch:public AlignmentAlgorithm {
public:
    
    using AlignmentAlgorithm::AlignmentAlgorithm;
    void FindAlingment();
};

#endif /* NeedlemanWunsch_hpp */
