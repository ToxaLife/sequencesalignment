//
//  Header.h
//  SequencesAlignment
//
//  Created by Александр Поповкин on 30/10/15.
//  Copyright © 2015 AlexStudio. All rights reserved.
//

#ifndef Header_h
#define Header_h

#pragma once
#include <iostream>
#include "AlignmentAlgorithm.hpp"
#include "NeedlemanWunsch.hpp"
#include "SmithWaterman.hpp"

#define MATCH_NUMBER 10
#define MISMATCH_NUMBER -8
#define GAP_OPEN -10
#define GAP_EXTEND -1

#endif /* Header_h */
