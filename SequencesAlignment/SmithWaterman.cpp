//
//  SmithWaterman.cpp
//  SequencesAlignment
//
//  Created by Александр Поповкин on 30/10/15.
//  Copyright © 2015 AlexStudio. All rights reserved.
//

#include "SmithWaterman.hpp"

using namespace std;

void SmithWaterman::FindAlingment() {
    int i, j;
    string align_a = "", align_b = "";
    
    build_score_matrix(true);
    
    tie (i, j, result_score) = max_matrix();
    cout << "Max element found\n";
    while (score_matrix(i, j).score > 0) {
        int score = score_matrix(i, j).score;
        int score_diag = score_matrix(i - 1, j - 1).score;
        int _x = score_matrix(i, j).x;
        int _y = score_matrix(i, j).y;
        
        if (i > 0 && j > 0 && score == score_diag + compare_input_strings(i - 1, j - 1)) {
            align_a = input_string_1[--i] + align_a;
            align_b = input_string_2[--j] + align_b;
            
        }
        else if ( i > 0 && score == _y) {
            align_a = input_string_1[--i] + align_a;
            align_b = "-" + align_b;
            
        }
        else if (j > 0 && score == _x){
            align_a = "-" + align_a;
            align_b = input_string_2[--j] + align_b;
        }
    }
    
    result_string_1 = input_string_1[0] + align_a;
    result_string_2 = input_string_2[0] + align_b;
}

tuple<int, int, int> SmithWaterman::max_matrix() {
    int i_max = 0, j_max = 0, s_max = score_matrix(0, 0).score;
    
    for (int i = 0; i < (int)score_matrix.size1(); i++) {
        for (int j = 0; j < (int)score_matrix.size2(); j++) {
            if (score_matrix(i, j).score >= s_max) {
                s_max = score_matrix(i, j).score;
                i_max = i;
                j_max = j;
            }
        }
    }
    
    return make_tuple(i_max, j_max, s_max);
}